package nz.co.wanghao8522;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;



public class LoginFrame extends JFrame{


	JButton jButton = null;
	JLabel  jLabel = null;
	JTextField jTextfield = null;

	public static void main(String args[]){
		
		
		new LoginFrame();
		
	}
	
	// close login frame
	
	private void close(){
		
		this.setVisible(false);
	}
	
	
	public LoginFrame(){
		
		
		this.setLocation(100, 100);
		this.setSize(400,100);
		this.setResizable(false);
		this.setTitle("Login");
		this.setLayout(new FlowLayout());
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
		jButton = new JButton("Connect");
		
		jButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				String name = jTextfield.getText();
			

				if(name== null ||name.trim().equals("")){
					
					JOptionPane.showMessageDialog(null, "Please input username!");
					
					return;
					
				}
		
				new ClientFrame(name);
				
				close();
				
				
				
				
				
			}
			
			
			
		});
		
		jLabel = new JLabel("Username:");
		
		jTextfield = new JTextField(25);
		
	
		
		
		this.add(jLabel);
		this.add(jTextfield);
		this.add(jButton);
		
		
		this.setVisible(true);
	}
	
	
	
}
