package nz.co.wanghao8522;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ClientFrame extends JFrame{

	
	JTextArea jTextArea = null;
	JButton jButton = null;
	JPanel jPanel = null;
	JScrollPane jScrollPane1,jScrollPanel2;
	JList list = null;
	JTextField jTextField = null;
	private String name;
	private DefaultListModel model;
   
	Socket s = null;  
 
	BufferedReader  br;
	
	PrintWriter out;
	
	private boolean stop = false;
	
	
	public ClientFrame(String name){
		
		this.setLocation(200,200);
		
		this.name = name;
		
		this.setTitle("Current User: "+this.name);
		this.setSize(600, 400);
		
		
		// close window
		this.addWindowListener( new WindowAdapter(){
			
			public void windowClosing(WindowEvent e){
				
				out.println(Server.DISCONNECT_TOKEN);
			}
			
		});
		
		
		jTextArea = new JTextArea();
		
		jScrollPane1 = new JScrollPane(jTextArea);
		
		jButton = new JButton("Send");
		
		jButton.addActionListener(new ClickButton());
		
		jTextField = new JTextField(25);
		
		jTextField.addKeyListener(new KeyClick());
		
		jPanel  = new JPanel();
		
		model = new DefaultListModel();
		
		list = new JList(model);
		
		
		list.setFixedCellWidth(100);
		
		jScrollPanel2 = new JScrollPane(list);
		
	
		
		this.add(jScrollPane1);
		
	    jPanel.add(jTextField);
	    
	    jPanel.add(jButton);
	    
		this.add(jPanel,BorderLayout.SOUTH);
		
		this.add(jScrollPanel2,BorderLayout.WEST);
		
		connect();
		
		this.setVisible(true);
	}
	
	
	private void connect(){
		
		
		try {
			
			s = new Socket(Server.HOST,Server.PORT);
			
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			
			out = new PrintWriter(s.getOutputStream(),true);
			
			out.println(name);
			
			new Thread(new ReceiveThread()).start();
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void send(){
		
		
		String word = jTextField.getText();
		
		
		if(word == null|| word.trim().equals("")){
			
			return;
		}
		
		// selected user
		
		Object [] vals = list.getSelectedValues();
		
		boolean isAll = false;
		
		String us = Server.CHAT_FLAG_START;
		
		for(Object v :vals){
			
			if(vals.length<= 0) isAll = true;
			
			if(v.equals("All")){
				
				isAll = true;
				break;
				
			}else{
				
				us+=v.toString()+",";
			}
			
		}
		
		
		if(isAll){
			
			out.println(us+"all"+Server.CHAT_FLAG_END +word);
		} else{
			
			out.println(us+Server.CHAT_FLAG_END +word);
		}
		
  
		jTextField.setText("");
		
	}
	
	private void close(){
		
		stop =true;
		
	}
	
	private void handList(String str){
		
		String users = str.substring(Server.TRAN_USER_FLAG.length());
		
		String us [] = users.split(",");
		
		model.removeAllElements();
		
		for(String u :us){
			
			model.addElement(u);
		}
		
		model.addElement("All");
		
	}
	
	
	private void receive() throws IOException{
		
		String str = br.readLine();
		
		if(str.equals(Server.DISCONNECT_TOKEN)){
			
			close();
			
		}
		
		System.out.println(str);
		
		
		if(str.startsWith(Server.TRAN_USER_FLAG)){
			
			handList(str);
			 
			return;
			
		}
		
		
		jTextArea.setText(jTextArea.getText()+str+"\n");
		
		
	}
	
	
	
	private class KeyClick extends KeyAdapter{
		
		
		public void keyPressed(KeyEvent e){
			
			if(e.getKeyCode() ==KeyEvent.VK_ENTER){
				
				send();
			}
			
		}
		
	}
	
	
	
	private class ClickButton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			
			if(e.getSource() ==jButton){
				
			   send();
	
			   
			}
			
			
		}
		
	}
	
	private class ReceiveThread implements Runnable{
		
		

		@Override
		public void run() {
			
			
			try {
				while(true){
					
					if(stop)break;
					
					 receive();
					
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				
			
					try {
						
						
						if(s!=null)
						 s.close();
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					System.exit(0);
				
			}
		}
		
		
	}
	
}
