package nz.co.wanghao8522;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Server {
	
	
	public static final  int PORT = 8888;
	public static final String HOST="127.0.0.2";
	public static final String DISCONNECT_TOKEN ="disconnect";
	private Map<String,ServerThread>  cs;
	public static final String TRAN_USER_FLAG = "connect:";

	public static final String CHAT_FLAG_START ="to:";
	public static final String CHAT_FLAG_END = ":end";
	
	
	public static void main(String args[]){
		
		  new Server().startUp();
		
		
	}
	
	private void startUp(){
		
  ServerSocket ss = null;
		  
		  
		  try {
			ss = new ServerSocket(PORT);
			
			cs = new HashMap<String,ServerThread>();
			
			while(true){
				
				try {
					
					Socket s = ss.accept();
					
					new Thread(new ServerThread(s)).start();
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
			 
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			
			
				try {
					if(ss!=null)  ss.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
	}
	
	
	private class ServerThread implements Runnable{

		
		private Socket s ;
		private BufferedReader br;
		private PrintWriter out;
		private String name;
		private boolean stop = false;
		
		
		public ServerThread(Socket s) throws IOException{
			
			    this.s = s;
			
	            br = new BufferedReader(new InputStreamReader(s.getInputStream()));
	            
	            out = new PrintWriter(s.getOutputStream(),true);
	            
	            name= br.readLine();
	            
	            name+= " ["+ s.getInetAddress().getHostAddress()+"]";
	            
	            send(name+" connected");
			
	            cs.put(name,this);
	            
	            sendUser();
		}
		
		
		private void send(String msg){
			
			Set<String> keys=cs.keySet();
			
			for(String key:keys){
				
				cs.get(key).out.println(msg);
			}
			
			
			
		}
		
		
		// user list
		private void sendUser(){
			
			String us =TRAN_USER_FLAG;
			
			Set<String> keys = cs.keySet();
			for(String u:keys){
				
				us+=u+",";
				
			}
			
			send(us);
			
		}
		
		
		private void close(){
			
			stop = true;
			
			out.println(DISCONNECT_TOKEN);
			
			//System.out.println(name+"disconnect");
			
			cs.remove(name);
			
			send(name+"disconnect");
			
			sendUser();
		}
		
		// filter message
		private String getUsersByMsg(String msg) {
			String str = msg.substring(CHAT_FLAG_START.length(),msg.indexOf(CHAT_FLAG_END));
			return str;
		}
		
		private String formatMsg(String msg) {
			String str = msg.substring(msg.indexOf(CHAT_FLAG_END)+CHAT_FLAG_END.length());
			return str;
		}
		
		@Override
		public void run() {
			
			
			
				
			
		   try {
					
			while(true){
				
				   
				    if(stop) break;
				
					String	str = br.readLine();
					
					System.out.println(name+":"+str);
					
					if(str.equals(DISCONNECT_TOKEN)){
						
						close();
						break;
						
					}
					
					String us = getUsersByMsg(str);
					String msg = formatMsg(str);
					if(us.equals("all")) {
						send(name+":"+msg+"[Group]");
					} else {
						sendPrivateUsers(us,msg);
					}
					
			
					
					
				}
					
					
					
				} catch(SocketException e){
					
					System.out.println(name+" close the client frame");
					close();
					
				}catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally{
					
					
						try {
							
							if(s!=null)
							s.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}
		
		}


		private void sendPrivateUsers(String us, String msg) {
			String [] uus = us.split(",");
			for(String u:uus) {
				cs.get(u).out.println(name+":"+msg+"[Private]");
			}
		}
		
		
	}
	 

}



